import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

public class Numbers {

    private static final String NUMBERFILE_PATH = "test_data.txt";
    private static final int MAX_NUMBER = 20;

    //Создаёт файл в котором есть значения от 0 до 20 в произвольном порядке
    //Значения указаны через запятую
    private static void CreateNumberFile() {
        List<Integer> numbers = new LinkedList<>();
        for (int number = 0; number <= MAX_NUMBER; number++){
            numbers.add(number);
        }
        Collections.shuffle(numbers);
        try {
            FileWriter fw = new FileWriter(NUMBERFILE_PATH);
            for (int i = 0; i <= MAX_NUMBER; i++) {
                fw.write(numbers.get(i).toString());
                if (i == MAX_NUMBER) continue;
                fw.write(',');
            }

            fw.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void PrintNumbers(Comparator comparator){
        try{
            CreateNumberFile();
            FileReader fr= new FileReader(NUMBERFILE_PATH);
            Scanner scan = new Scanner(fr);

            List<Integer> numbers = new LinkedList<>();

            String fileText = scan.hasNext() ? scan.next() : "";
            String[] stringNumbers = fileText.split(",");

            for (String stringNumber: stringNumbers){
                numbers.add(Integer.valueOf(stringNumber));
            }

            numbers.sort(comparator);

            for (Integer number : numbers){
                System.out.println(number);
            }
            scan.close();
            fr.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        CreateNumberFile();
        System.out.println("1. Прочитай значения из файла, отфильтруй значения по возрастанию и выведи в консоль.");
        PrintNumbers(Comparator.naturalOrder());
        System.out.println("2. Прочитай значения из файла, отфильтруй значения по убыванию и выведи в консоль.");
        PrintNumbers(Comparator.reverseOrder());
    }

}
