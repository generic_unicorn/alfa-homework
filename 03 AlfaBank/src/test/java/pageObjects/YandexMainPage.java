package pageObjects;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class YandexMainPage {
    public YandexMainPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, id = "text")
    private WebElement requestInput;

    @FindBy(how = How.XPATH, xpath = "//button[span[text() = 'Найти']]")
    private WebElement searchButton;

    public void search(String searchRequest) {
        requestInput.sendKeys(searchRequest);
        searchButton.click();
    }
}
