package pageObjects;

        import java.util.ArrayList;
        import java.util.List;

        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.interactions.Actions;
        import org.openqa.selenium.support.FindAll;
        import org.openqa.selenium.support.FindBy;
        import org.openqa.selenium.support.How;
        import org.openqa.selenium.support.PageFactory;
        import org.openqa.selenium.support.ui.ExpectedConditions;
        import org.openqa.selenium.support.ui.WebDriverWait;

public class AlfaBankMainPage {
    public AlfaBankMainPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, 6);
        PageFactory.initElements(driver, this);
    }

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(how = How.XPATH, xpath = "//a[@title = 'Вакансии']")
    private WebElement jobs;

    public void goToJobs() {
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        driver.switchTo().window((String) tabs.get(1));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@title = 'Ипотека']")));
        Actions actions = new Actions(driver);
        actions.moveToElement(jobs);
        actions.perform();
        jobs.click();
    }
}

