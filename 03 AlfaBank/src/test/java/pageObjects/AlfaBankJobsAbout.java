package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class AlfaBankJobsAbout {
    public AlfaBankJobsAbout(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, 6);
        PageFactory.initElements(driver, this);
    }

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(how = How.XPATH, xpath = "//div[h3[text() = 'О нас']]/div/p")
    List<WebElement> paragraphs;

    public String getParagraphsText() {
        String result = "";
        for (int i = 0; i < paragraphs.size(); i++) {
            result += paragraphs.get(i).getText();
            result += "\n";
        }
        return result;
    }
}
