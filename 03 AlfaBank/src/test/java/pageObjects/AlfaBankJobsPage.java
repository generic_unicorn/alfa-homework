package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AlfaBankJobsPage {
    public AlfaBankJobsPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, 6);
        PageFactory.initElements(driver, this);
    }

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(how = How.XPATH, xpath = "//a[text() = 'О нас']")
    private WebElement about;

    public void goToAbout() {
        about.click();
    }
}