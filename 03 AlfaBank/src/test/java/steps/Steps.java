package steps;


import cucumber.api.java.en.Then;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import org.openqa.selenium.remote.RemoteWebDriver;
import pageObjects.*;
import test.utils.*;

import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class Steps {
    private WebDriver driver;

    @When("^пользователь переходит в поисковую систему yandex$")
    public void user_goes_to_yandex() {
        driver = DriverHelper.getChromeDriver();
        driver.navigate().to("https://yandex.ru");
    }

    @And("^пользователь ищет сайт АльфаБанка$")
    public void user_search_alfabank() {
        YandexMainPage yandex = new YandexMainPage(driver);
        TestContext.addToContext(ConstantHelper.SEARCH_ENGINE_KEY, driver.getTitle());
        yandex.search("альфа-банк");
    }

    @And("^пользователь открывает результат номер \"(.*)\"$")
    public void user_clicks_on_result(String resultNum) {
        YandexResultsPage resultPage = new YandexResultsPage(driver);
        resultPage.clickOnResult(Integer.valueOf(resultNum));
    }

    @And("^пользователь открывает вакансии$")
    public void user_goes_to_jobs() {
        AlfaBankMainPage alfaBankMainPage = new AlfaBankMainPage(driver);
        alfaBankMainPage.goToJobs();
    }

    @And("^пользователь переходит на страницу О нас$")
    public void user_goes_to_about() {
        AlfaBankJobsPage alfaBankJobsPage = new AlfaBankJobsPage(driver);
        alfaBankJobsPage.goToAbout();
    }

    @Then("^пользователь запоминает текст абзацев О нас$")
    public void user_remembers_about() {
        AlfaBankJobsAbout jobsAbout = new AlfaBankJobsAbout(driver);
        String paragraphs = jobsAbout.getParagraphsText();
        TestContext.addToContext(ConstantHelper.ABOUT_KEY, paragraphs);
    }

    @Then("^пользователь сохраняет текст в файл$")
    public void user_saves_text_to_file() {
        String file_name = "";

        //добавить дату и время
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm.ss");
        LocalDateTime now = LocalDateTime.now();
        file_name+=dtf.format(now);
        file_name+=" ";

        //добавить имя браузера
        Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
        file_name += caps.getBrowserName();
        file_name+=" ";

        //добавить имя поисковой системы
        if(TestContext.hasValueFor(ConstantHelper.SEARCH_ENGINE_KEY)){
            file_name += TestContext.getValue(ConstantHelper.SEARCH_ENGINE_KEY);
            file_name+=".txt";
        }

        try {
            FileWriter fw = new FileWriter(file_name);
            if(TestContext.hasValueFor(ConstantHelper.ABOUT_KEY)){
                fw.write(TestContext.getValue(ConstantHelper.ABOUT_KEY).toString());
            }
            fw.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Then("^очистить контекст$")
    public void clearContext() {
        TestContext.clearContext();
    }
}