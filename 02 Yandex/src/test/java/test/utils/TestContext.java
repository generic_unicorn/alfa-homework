package test.utils;

import java.util.HashMap;
import java.util.Map;

public class TestContext {

    private static Map<String, Object> testContext;

    static {
        testContext = new HashMap<String, Object>();
    }

    public static void addToContext(String key, Object value) {
        testContext.put(key.toString(), value);
    }

    public static Object getValue(String key){
        return testContext.get(key);
    }

    public static Boolean hasValueFor(String key){
        return testContext.containsKey(key);
    }

    public static void clearContext(){
        testContext.clear();
    }
}