package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductsPage {
    private WebDriver driver;

    public ProductsPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, 5);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, id = "glpricefrom")
    private WebElement priceMin;

    @FindBy(how = How.ID, id = "glpriceto")
    private WebElement priceMax;

    @FindBy(how = How.XPATH, xpath = "//fieldset[legend[text() = 'Производитель']]")
    private WebElement brandList;

    private WebDriverWait wait;

    public void setPrice(String min, String max) {
        if(min!=null && !min.isEmpty()) {
            priceMin.sendKeys(min);
            wait.until(ExpectedConditions.textToBePresentInElementValue(priceMin, min));
        }
        if(max!=null && !max.isEmpty()) {
            priceMax.sendKeys(max);
            wait.until(ExpectedConditions.textToBePresentInElementValue(priceMax, max));
        }

        //ждем, пока старые товары исчезнут и появятся новые
        wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("(//div[contains(@data-id,'model')])[1]"))));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[contains(@data-id,'model')])[1]")));
    }

    public WebElement selectElement(int number) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//div[contains(@data-id,'model')])[" + number + "]")));
        WebElement product = driver.findElement(By.xpath("(//div[contains(@data-id,'model')])[" + number + "]"));
        return product;
    }

    public void selectBrand(String brand){
        WebElement brandCheckbox;
        if(brandList.getText().contains(brand)){
            brandCheckbox = brandList.findElement(By.xpath(".//span[text() = '" + brand + "']"));
        } else{
            brandList.findElement(By.xpath(".//a[text() = 'Показать всё']")).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name = 'Поле поиска']")));
            WebElement brandInput = driver.findElement(By.xpath("//input[@name = 'Поле поиска']"));
            brandInput.sendKeys(brand);
            brandCheckbox =
                driver.findElement
                (By.xpath(("//input[contains(@name, '" + brand + "')]/parent::node()/div/span[text() = '" + brand + "']")));
        }
        brandCheckbox.click();
    }
}
