package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MarketPage {

    private WebDriver driver;

    public MarketPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, xpath = "//div/div/span[text() = 'Все категории']")
    private WebElement categoriesButton;



    public void chooseCategory(String category, String subCategory) {
        categoriesButton.click();
        WebElement categoryElement = driver.findElement(By.xpath("//a[img]/span[text() = '" + category + "']"));
        Actions builder = new Actions(driver);
        builder.moveToElement(categoryElement).build().perform();
        WebElement subCategoryElement = driver.findElement(By.xpath("//a[text() = '" + subCategory + "']"));
        subCategoryElement.click();
    }
}
