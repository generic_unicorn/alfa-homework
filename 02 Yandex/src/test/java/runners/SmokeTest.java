package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import test.utils.DriverHelper;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features",
        glue = "steps",
        tags = "@smoketest"
)

public class SmokeTest {
    @AfterClass()
    public static void kill_browser(){
        try{
        DriverHelper.getChromeDriver().quit();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}

