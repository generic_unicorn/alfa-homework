package steps;

import java.util.concurrent.TimeUnit;


import cucumber.api.java.en.Then;
import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import pageObjects.MainPage;
import pageObjects.MarketPage;
import pageObjects.ProductsPage;
import pageObjects.SingleProductPage;
import test.utils.*;


public class Steps {
    private WebDriver driver;

    @Given("^пользователь переходит на главную страницу$")
    public void user_goes_to_Home_Page() {
        driver = DriverHelper.getChromeDriver();
        driver.navigate().to("https://yandex.ru");
    }

    @When("^пользователь перехоит на вкладку маркет$")
    public void user_navigates_to_market_tab() {
        MainPage mainPage = new MainPage(driver);
        mainPage.goToMarket();
    }

    @And("^пользователь перехоит в раздел \"(.*)\", подраздел \"(.*)\"$")
    public void user_navigates_to_sub_category(String category, String subCategory) {
        MarketPage marketPage = new MarketPage(driver);
        marketPage.chooseCategory(category, subCategory);
    }

    @And("^пользователь вводит цену от \"(.*)\" до \"(.*)\"$")
    public void user_sets_price(String minPrice, String maxPrice) {
        ProductsPage productsPage = new ProductsPage(driver);
        productsPage.setPrice(minPrice, maxPrice);
    }

    @And("^пользователь запоминает имя продукта номер \"([1-9]d*)\"$")
    public void user_remembers_product_name(String product_num) {
        int num = Integer.valueOf(product_num);
        ProductsPage productsPage = new ProductsPage(driver);
        WebElement product = productsPage.selectElement(num);
        String productName = (product.findElement(By.xpath("./div/div/a"))).getText();
        TestContext.addToContext(ConstantHelper.PRODUCT_KEY, productName);
    }

    @And("^пользователь переходит в продукт номер \"([1-9]d*)\"$")
    public void user_navigates_into_product(String product_num) {
        int num = Integer.valueOf(product_num);
        ProductsPage productsPage = new ProductsPage(driver);
        WebElement product = productsPage.selectElement(num);
        product.click();
    }

    @And("^пользователь выбирает производителя \"(.*)\"$")
    public void user_selects_brand(String brandName) {
        if(brandName != null && !brandName.isEmpty()) {
            ProductsPage productsPage = new ProductsPage(driver);
            productsPage.selectBrand(brandName);
        }
    }

    @Then("^имя продукта совпадает с тем, которое пользователь запомнил ранее$")
    public void check_product_same_name(){
        SingleProductPage singleProductPage = new SingleProductPage(driver);
        String expected;
        if(TestContext.hasValueFor(ConstantHelper.PRODUCT_KEY)){
            expected = (String) TestContext.getValue(ConstantHelper.PRODUCT_KEY);
        } else {
            expected = "";
        }
        String actual = singleProductPage.getTitle();
        Assert.assertEquals(expected,actual);
    }

    @When("^очистить контекст$")
    public void clearContext() {
        TestContext.clearContext();
    }
}